const imagenes = document.querySelectorAll('.imagen');

for(imagen of imagenes){
    arrastrarElemento(imagen);
}

function arrastrarElemento(imagen){

    let pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    imagen.onpointerdown = arrastrarPuntero;
    imagen.style.cursor = "move";
    

    function arrastrarPuntero(e) {

        e.preventDefault();

        pos3 = e.clientX;
        pos4 = e.clientY;

        document.onpointermove = iniciarArrastreElemento;
        document.onpointerup = detenerArrastreElemento;

    }

    function iniciarArrastreElemento(e) {

        
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        imagen.style.top = (imagen.offsetTop - pos2) + "px";
        imagen.style.left = (imagen.offsetLeft - pos1) + "px";
        imagen.style.position = "absolute";

    }

    function detenerArrastreElemento(){
        
        document.onpointerup = null;
        document.onpointermove = null;

    }

}

